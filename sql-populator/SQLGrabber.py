import time
from ib_insync import *
from dateutil.relativedelta import relativedelta
import pandas as pd 
import datetime
from sqlalchemy import create_engine
import pymysql
import mainlib
import fundamental_parser
import thread




# connect to ib 
app = IB()
app.connect('127.0.0.1', 7497, clientId = 3, timeout = 5)

if app.client.isReady() == True:
    print("We're ready to pull data")
else:
    print("API is having issues connecting!")
    app.disconnect()

def get_tickers():
    '''
    Opens the ticker files and extracts tickers into a list so they can be iterated through and requested 

    return: 
        a list of tickers to make requests for
    '''
    tickers = []
    
    nyse_file = open('NYSE.txt', 'r')
    nyse_lines = nyse_file.readlines()
    # loop through nyse and add to list
    for line in nyse_lines:
        if not 'Tickers' in line and not line == '\n':
            # we have a ticker and name set, add ticker
            tickers.append(line.split(':')[1].strip('\n'))

    amex_file = open('AMEX.txt', 'r')
    amex_lines = amex_file.readlines()
    # loop through amex and add to list
    for line in amex_lines:
        if not 'Tickers' in line and not line == '\n':
            # we have a ticker and name set, add ticker
            tickers.append(line.split(':')[1].strip('\n'))

    
    nasdaq_file = open('NASDAQ.txt', 'r') 
    nasdaq_lines = nasdaq_file.readlines()
    # loop through amex and add to list
    for line in nasdaq_lines:
        if not 'Tickers' in line and not line == '\n':
            # we have a ticker and name set, add ticker
            tickers.append(line.split(':')[1].strip('\n'))

    return tickers
# open and save the tickers we want to get data for

#creating dataprovider object

def in_between(now, start, end):
    '''
    Checks if now is inbetween start and end

    '''
    if start <= end:
        return start <= now < end
    else: # over midnight e.g., 23:30-04:15
        return start <= now or now < end


def request_time_condition(request_time):

    '''
    Changes request time every 1 second for ticks during market hours
    Pulls request time every 30 seconds for ticks after market hours 

    '''

    if in_between(request_time.time(), datetime.time(8, 30), datetime.time(15)):
        condition1 = True
    else:
        condition1 = False
    
    if request_time.today().weekday() == 0 or 1 or 2 or 3 or 4:
        condition2 = True
    else:
        condition2 = False
    
    if condition1 and condition2 == True:
        weekday_condition = True
        return weekday_condition
    else:
        weekday_condition = False
        return weekday_condition

    
    return weekday_condition

def request_stopping_condition(request_time, launch_time, lower_catch, upper_catch):

    '''
    Handles exception where the request time surpasses the launch time and the process needs to then terminate so the next ticker can be called

    Parameters:

    request_time (datetime): where the while loops current request is at 
    launch_time (datetime): the current time
    lower_catch(int): n seconds below launch_time threshold to end loop
    upper_catch(int): n seconds above launch_time threshold to end loop

    '''

    distance = relativedelta(request_time, launch_time)

    if distance == relativedelta(seconds = lower_catch) or distance == relativedelta(seconds = upper_catch):
        stopping_time = True
    else:
        stopping_time = False

    print(distance)
    
    return stopping_time

def make_cursor(database_name):
    '''
    Takes a database name and returns a cursor for connecting to and editing the database

    params:
        database_name (string): name of the database
    '''
    text = open('keys.txt', 'r')
    content = str(text.read())
    user = content.split('<')[1].split('>')[0]
    password = content.split('<')[2].split('>')[0]

    # TODO: change this to be  sQLAlchemy not mysql connector

    db = create_engine("mysql://"+user+':'+password+"@localhost/"+database_name + "?unix_socket=/var/run/mysqld/mysqld.sock", pool_pre_ping=True) 
    
    return db

def request_level_one_trades(ticker, launch_time, request_time, dataframe):
    '''
        Used for making requests by threads for a singular ticker. The thread is given a copy of the dataframe, launch, and request time in order to loop through all time points from 6 months ago to now to populate a dataframe, create a sql table, and add the table to the databse
        
        params:
            ticker(string): represents ticker being requested for 
            launch_time(datetime): represents the time that this function will be called at 
            request_time(datetime): represents the time that the data is getting requested for (starts six months ago)
            dataframe(pd.DataFrame): used to store the requested data before transfering to sql database
    '''
    contract = mainlib.grab_contract(asset_class = 'Stock', ticker=ticker, option_type = None, option_strike = None, option_expiry = None, app=app)

   
    #Initializing fencepost variable last and stopping time condition 

    last = None
    stopping_time = False

    #loops through request time to launch time and checks for conditions to dynamically end loop or change the frequency of requests

    while not request_time == launch_time and stopping_time == False:
        print(request_time)
        now =  app.reqHistoricalTicks(contract= contract, startDateTime = '', endDateTime= request_time, numberOfTicks=1000, whatToShow = 'Trades', useRth = True)

        # checks if there's a duplicate dataframe: if duplicate dont concatenate
        if not now == last:
            data = util.df(now)
            frames = [dataFrame, data]
            dataFrame = pd.concat(frames, sort = False, ignore_index = True)
            print(dataFrame)
        else:
            pass 

        # stores dataframe for next iteration, checks weekday condition

        last = now

        weekday_condition = request_time_condition(request_time)

        #initialize weekday condition interval 

        small_increment = datetime.timedelta(seconds = 1)
        big_increment = datetime.timedelta(seconds = 30)  

        # Conditions lower_catch/upper_catch on whether or not we are in normal tradinghours (condition == True)

        if weekday_condition == True:
            request_time = request_time + small_increment
            lower_catch = -(small_increment.seconds)
            upper_catch = small_increment.seconds
        else:
            request_time = request_time + big_increment    
            lower_catch = -(big_increment.seconds)
            upper_catch = big_increment.seconds 

        #checks stopping condition to handle when the request time is not exactly launch time, rather its within a lower bound and upper bound (lower_catch/upper catch)
        stopping_time = request_stopping_condition(request_time, launch_time, lower_catch = lower_catch, upper_catch = upper_catch)
    
    print(dataFrame.head())
    #print of the dataframe for ticker i and then push it to SQL database/csv/etc.
    if output_method == 'csv':
        filename = str(ticker+'tradesdata.csv')
        dataFrame.to_csv(filename, index = False)
    elif output_method == 'df':
        #TODO: Make unique dataframe for every ticker
        print(dataFrame.info())
    else:
        #create SQL table
        table_name = ticker + "-trade_data"
        with engine.connect() as connection:
            connection.execute('CREATE TABLE IF NOT EXISTS '+table_name+'(time DATETIME, tickAttribLast VARCHAR(25) , price FLOAT(5), size INTEGER, exchange VARCHAR(6), specialConditions VARCHAR(5), UNIQUE(time, tickAttribLast, price, size, exchange, specialConditions));')
        try:
            dataFrame.to_sql(table_name, connection, if_exists ='fail')
        except ValueError as vx:
            print(vx)
        except Exception as ex:
            print(ex)
        finally:
            print("Table %s created successfully."%table_name)
            connection.close()

# Level One Trade Data


def request_all_level_one_trades(tickers, output_method):

    '''
    Iteratively grabs tick data ('trades') for each ticker item in the list of tickers

    Parameters:

    tickers(list): Generated in get_tickers: A list of tickers to iterate this process over

    output_method(str): 'SQL' (default), 'df' (pandas dataframe), 'csv' (csv): Choose your output method.


    '''

    #Create SQL object
    engine = make_cursor('Level-One-Data')
    
    for i in range(len(tickers)):
        # Find start and end time
        # Create DataFrame and contract object

        launch_time = datetime.datetime.now()
        request_time = launch_time + relativedelta(months = -6) 

        dataFrame = pd.DataFrame(columns = ['time','tickAttribLast','price','size','exchange','specialConditions'])

        # here is where we start changing for diff threads

        # TODO: need to define the first second and third ticker
       
        # if no more elements 
        if i >= len(tickers):
            break
        # if we are on the last element 
        elif i == len(tickers) - 1:
            # only call one thread
            thread.start_new_thread(request_level_one_trades, (ticker[i]), request_time, launch_time, dataframe))
        # if we are on second to last 
        elif i == len(tickers) - 2:
            thread.start_new_thread(request_level_one_trades, (ticker[i], request_time, launch_time, dataframe))
            thread.start_new_thread(request_level_one_trades, (ticker[i+1], request_time, launch_time, dataframe))
        # if we can afford to call all three threads
        else:
            thread.start_new_thread(request_level_one_trades, (ticker[i], request_time, launch_time, dataframe))
            thread.start_new_thread(request_level_one_trades, (ticker[i+1], request_time, launch_time, dataframe))
            thread.start_new_thread(request_level_one_trades, (ticker[i+2], request_time, launch_time, dataframe))

        # TODO: increment i by the correct value

def request_level_one_bidask(ticker, request_time, launch_time, dataframe):
    '''
        Used for making requests by threads for a singular ticker. The thread is given a copy of the dataframe, launch, and request time in order to loop through all time points from 6 months ago to now to populate a dataframe, create a sql table, and add the table to the databse
        
        params:
            ticker(string): represents ticker being requested for 
            launch_time(datetime): represents the time that this function will be called at 
            request_time(datetime): represents the time that the data is getting requested for (starts six months ago)
            dataframe(pd.DataFrame): used to store the requested data before transfering to sql database
    '''
    contract = mainlib.grab_contract(asset_class = 'Stock', ticker=ticker, option_type = None, option_strike = None, option_expiry = None, app=app)
    
    #Initializing fencepost variable last and stopping time condition 

    last = None
    stopping_time = False

    while not request_time == launch_time and stopping_time == False: 
        print(request_time)
        now =  app.reqHistoricalTicks(contract= contract, startDateTime = request_time, endDateTime='', numberOfTicks=1000, whatToShow = 'Bid_Ask', useRth = True)
        
        # checks if there's a duplicate dataframe: if duplicate dont concatenate
        if not now == last:
            data = util.df(now)
            frames = [dataFrame, data]
            dataFrame = pd.concat(frames, sort = False, ignore_index = True)
            print(dataFrame)
        else:
            pass 
    
        #stores dataframe for next iteration, cheks weekday condition
        last = now

        weekday_condition = request_time_condition(request_time)

        #initialize weekday condition interval

        small_increment = datetime.timedelta(seconds = 1)
        big_increment = datetime.timedelta(seconds = 30)  

        if weekday_condition == True:
            request_time = request_time + small_increment
            lower_catch = -(small_increment.seconds)
            upper_catch = small_increment.seconds
        else:
            request_time = request_time + big_increment    
            lower_catch = -(big_increment.seconds)
            upper_catch = big_increment.seconds     

        #checks stopping condition to handle when the request time is not exactly launch time, rather its within a lower bound and upper bound (lower_catch/upper catch)

        stopping_time = request_stopping_condition(request_time, launch_time, lower_catch = lower_catch, upper_catch = upper_catch)


    #print of the dataframe for ticker i and then push it to SQL database/csv/etc.
    if output_method == 'csv':
        filename = str(ticker+'bidaskdata.csv')
        dataFrame.to_csv(filename, index = False)
    elif output_method == 'df':
        #TODO: Make unique dataframe for every ticker
        print(dataFrame.info())
    else:
        #create SQL table in all other cases and by default
        table_name = ticker + "-bidask_data"
        with engine.connect() as connection:
            connection.execute('CREATE TABLE IF NOT EXISTS '+table_name+'(priceBid FLOAT(3), priceAsk FLOAT(3), sizeBid INTEGER, sizeAsk INTEGER, time DATETIME, tickAttribBidAsk VARCHAR(48), UNIQUE(priceBid, priceAsk, sizeBid, sizeAsk, time, tickAttribBidAsk));')   
        try:
            dataFrame.to_sql(table_name, connection, if_exists ='fail')
        except ValueError as vx:
            print(vx)
        except Exception as ex:
            print(ex)
        finally:       
            print("Table %s created successfully."%table_name)
            connection.close()



def request_all_level_one_bidask(tickers, output_method):
    '''
    Iteratively grabs tick data ('bidask') for each ticker item in the list of tickers

    Parameters:

    tickers(list): Generated in get_tickers: A list of tickers to iterate this process over

    output_method(str): 'SQL' (default), 'df' (pandas dataframe), 'csv' (csv): Choose your output method.

    '''
    #Create SQL object
    engine = make_cursor('Level-One-Data')
    
    for i in range(len(tickers)):
        # FIND START AND END TIME
        # Create DataFrame and contract object

        launch_time = datetime.datetime.now()
        request_time = launch_time + relativedelta(days = -6, hours = 12)

        dataFrame = pd.DataFrame(columns = ['priceBid','priceAsk','sizeBid','sizeAsk'])
        
        if i>= len(tickers):
            break
        elif i == len(tickers) - 1:
            thread.start_new_thread(request_level_one_bidask, (ticker[i], request_time, launch_time, dataframe)
        elif i == len(tickers) -2:
            thread.start_new_thread(request_level_one_bidask, (ticker[i], request_time, launch_time, dataframe)
            thread.start_new_thread(request_level_one_bidask, (ticker[i+1], request_time, launch_time, dataframe)
        else;  
            thread.start_new_thread(request_level_one_bidask, (ticker[i], request_time, launch_time, dataframe)
            thread.start_new_thread(request_level_one_bidask, (ticker[i+1], request_time, launch_time, dataframe)
            thread.start_new_thread(request_level_one_bidask, (ticker[i+2], request_time, launch_time, dataframe)
             

def grab_fundamentals(tickers, report_type, analyst_type, output_method):

    '''
    Iteratively grabs fundamental data for each ticker item in the list of tickers

    Parameters: 

    tickers(list): Generated in get_tickers: A list of tickers to iterate this process over

    report_type(str): Report Type (ReportsFinSummary, ReportsOwnership, ReportsSnapshot, ReportsFinStatements, RESC)
    #TODO: RESC and a one time ReportsFinSummary need to be worked on

    analyst_type(str): If report_type == 'RESC': 'Estimated' or 'Actual'

    output_method(str): 'SQL' (default), 'df' (pandas dataframe), 'csv' (csv): Choose your output method.


    '''
    #Create SQL object
    engine = make_cursor('Fundamental_Data') #TODO: Make seperate cursors for each report type?
    
    for ticker in tickers:
        # Find start and end time
        # Create contract object
        contract = mainlib.grab_contract(asset_class = 'Stock', ticker=ticker, option_type = None, option_strike = None, option_expiry = None, app=app)

        if report_type == 'ReportsFinSummary':
            finsummary_dataframe = fundamental_parser.parseXML(contract = contract, report_type = 'ReportsFinSummary', analyst_type = None, news = False)
            if output_method == 'csv':
                filename = str(ticker+'reportsfinsummarydata.csv')
                finsummary_dataframe.to_csv(filename, index = False)
            elif output_method == 'df':
                #TODO: Make unique dataframe for every ticker
                print(finsummary_dataframe.info())
            else:
                #create SQL table in all other cases and by default
                table_name = ticker + "-reportsfinsummary_data"
                with engine.connect() as connection:
                    #TODO: Update this SQL Command
                    connection.execute('CREATE TABLE [IF NOT EXISTS] table_name( priceBid float(3), priceAsk float(3), sizeBid int(10), sizeAsk int(10), time datetime, tickAttribBidAsk varchar(48)')   
                try:
                    finsummary_dataframe.to_sql(table_name, connection, if_exists ='fail')
                except ValueError as vx:
                    print(vx)
                except Exception as ex:
                    print(ex)
                else:
                    print("Table %s created successfully."%table_name)
                finally:
                    connection.close()

        elif report_type == 'ReportsOwnership':
            ownership_dataframe = fundamental_parser.parseXML(contract = contract, report_type = 'ReportsOwnership', analyst_type = None, news = False)
            if output_method == 'csv':
                filename = str(ticker+'ownershipdata.csv')
                finsummary_dataframe.to_csv(filename, index = False)
            elif output_method == 'df':
                #TODO: Make unique dataframe for every ticker
                print(ownership_dataframe.info())
            else:
                #create SQL table in all other cases and by default
                table_name = ticker + "-ownership_data"
                with engine.connect() as connection:
                    #TODO: Update this SQL Command
                    connection.execute('CREATE TABLE [IF NOT EXISTS] table_name( priceBid float(3), priceAsk float(3), sizeBid int(10), sizeAsk int(10), time datetime, tickAttribBidAsk varchar(48)')   
                try:
                    finsummary_dataframe.to_sql(table_name, connection, if_exists ='fail')
                except ValueError as vx:
                    print(vx)
                except Exception as ex:
                    print(ex)
                else:
                    print("Table %s created successfully."%table_name)
                finally:
                    connection.close()

        elif report_type == 'ReportsSnapshot':
            dataframes = fundamental_parser.parseXML(contract = contract, report_type = 'ReportsSnapshot', analyst_type = None, news = False)
            ratios_dataframe = dataframes[0]
            forecast_dataframe = dataframes[1]
            if output_method == 'csv':
                filename1 = str(ticker+'ratiosdata.csv')
                filename2 = str(ticker+'forecastdata.csv')
                ratios_dataframe.to_csv(filename1, index = False)
                forecast_dataframe.to_csv(filename2, index = False)
            elif output_method == 'df':
                #TODO: Make unique dataframe for every ticker
                print(ratios_dataframe.info())
                print(forecast_dataframe.info())
            else:
                #create SQL table in all other cases and by default
                table_1_name = ticker + "-ratios_data"
                table_2_name = ticker + "-forecast_data"

                #ratios data
                with engine.connect() as connection:
                    #TODO: Update this SQL Command
                    connection.execute('CREATE TABLE [IF NOT EXISTS] table_1_name( priceBid float(3), priceAsk float(3), sizeBid int(10), sizeAsk int(10), time datetime, tickAttribBidAsk varchar(48)')   
                try:
                    ratios_dataframe.to_sql(table_1_name, connection, if_exists ='fail')
                except ValueError as vx:
                    print(vx)
                except Exception as ex:
                    print(ex)
                else:
                    print("Table %s created successfully."%table_1_name)
                finally:
                    connection.close()

                #forecast data
                with engine.connect() as connection:
                    #TODO: Update this SQL Command
                    connection.execute('CREATE TABLE [IF NOT EXISTS] table_2_name( priceBid float(3), priceAsk float(3), sizeBid int(10), sizeAsk int(10), time datetime, tickAttribBidAsk varchar(48)')   
                try:
                    forecast_dataframe.to_sql(table_2_name, connection, if_exists ='fail')
                except ValueError as vx:
                    print(vx)
                except Exception as ex:
                    print(ex)
                else:
                    print("Table %s created successfully."%table_1_name)
                finally:
                    connection.close()          
                         
        elif report_type == 'ReportsFinStatements':
            dataframes = fundamental_parser.parseXML(contract = contract, report_type = 'ReportsFinStatements', analyst_type = None, news = False)
            income_dataframe = dataframes[0]
            balance_dataframe = dataframes[1]
            cas_dataframe = dataframes[2]
        elif report_type == 'RESC':
            #fundamental_parser.parseXML(report_type = 'RESC', analyst_type = 'Actual' , news = False)
            print('Method currently unavailable')


        if output_method == 'csv':
            filename = str(ticker+'bidaskdata.csv')
            dataFrame.to_csv(filename, index = False)
        elif output_method == 'df':
            #TODO: Make unique dataframe for every ticker
            print(dataFrame.info())
        else:
            #create SQL table in all other cases and by default
            table_name = ticker + "-bidask_data"
            with engine.connect() as connection:
                connection.execute('CREATE TABLE [IF NOT EXISTS] table_name( priceBid float(3), priceAsk float(3), sizeBid int(10), sizeAsk int(10), time datetime, tickAttribBidAsk varchar(48)')   
            try:
                dataFrame.to_sql(table_name, connection, if_exists ='fail')
            except ValueError as vx:
                print(vx)
            except Exception as ex:
                print(ex)
            else:
                print("Table %s created successfully."%table_name)
            finally:
                connection.close()


def main():
    tickers = get_tickers()
    request_all_level_one_trades(tickers = tickers, output_method = 'SQL')
    request_all_level_one_bidask(tickers = tickers, output_method = 'SQL')

if __name__ == '__main__':
    main()
