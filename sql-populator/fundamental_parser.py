from ib_insync import *
from bs4 import BeautifulSoup
import lxml
import pandas as pd
import numpy as np
import datetime

fundamental_data = []
news_data = []

'''
def grab_contract(asset_class, ticker, option_type, option_strike, option_expiry):
    global contract 

    if asset_class == 'Stock':
        contract = Stock(ticker, 'SMART', 'USD')
    elif asset_class == 'option' and option_type == 'call':
        contract = Option(ticker, option_expiry, option_strike , 'C', 'SMART')
    elif asset_class == 'option' and option_type == 'put':
        contract = Option(ticker, option_expiry, option_strike , 'P', 'SMART')
    elif asset_class == 'Forex':
        contract = Forex(ticker)
    else:
        print('Error in identifying asset class identifier')

    return contract

'''

def parseXML(contract, report_type, analyst_type, news = bool):
    '''
    Parse XML data with BeautifulSoup

    (data = fundamental_data, report_type = 'RESC', analyst_type = 'Actual', news = False
        Parameters:

        contract(obj) = ib contract object
        report_type(str) = Report Type
        
        analyst_type(str) = 'Actual' or 'Estimate'

        news(bool) = True if news


    '''
    #connecting...  
    '''
    app = IB()
    app.connect('127.0.0.1', 7497, clientId = 4, timeout = 5)
        
    if app.client.isReady() == True:
        print("We're good to go!")
    else:
        print("API not connected!")
        app.disconnect()
    '''
    global fundamental_data
    global news_data

    


    

    

### REPORTS FIN SUMMARY (EPS/REVENUE)

    if report_type == 'ReportsFinSummary':
        fundamental_data = app.reqFundamentalData(contract = contract, reportType = report_type 
        ,fundamentalDataOptions= [])
        soup = BeautifulSoup(fundamental_data,'xml')
    
        eps_list = soup.find_all('EPS')
  
        revenue_list = soup.find_all('TotalRevenue')


        dataframe = pd.DataFrame(columns = ['Date','EPSPeriod', 'RevenuePeriod', 
        'Revenue', 'EPS'])

        for i in range(len(eps_list)):
            eps_entry = str(eps_list[i]) 
            revenue_entry = str(revenue_list[i])
            if not eps_entry == None:                                                     
                eps_date = eps_entry.split('"')[1]
                eps_period = eps_entry.split('"')[3]
                eps = eps_entry.split('>')[1].split('<')[0]        
            if not revenue_entry == None:
                revenue_date = revenue_entry.split('"')[1]
                revenue_period = revenue_entry.split('"')[3]
                revenue = revenue_entry.split('>')[1].split('<')[0]   
    
        dataframe.loc[i] = ({'Date':eps_date, 'EPSPeriod':eps_period, 'RevenuePeriod': revenue_period, 'Revenue': revenue, 'EPS' : eps})
        
        return dataframe
    
    
### REPORTS OWNERSHIP
  
    elif report_type == 'ReportsOwnership':
        fundamental_data = app.reqFundamentalData(contract = contract, reportType = report_type 
        ,fundamentalDataOptions= [])
        soup = BeautifulSoup(fundamental_data,'xml')
    
        purchase_type_list = soup.find_all('type')

        name_list = soup.find_all('name')
        

        quantity_list = soup.find_all('quantity')

        dataframe = pd.DataFrame(columns = ['purchase_type', 'name', 'date', 'quantity'])

        for i in range(len(name_list)):

            purchase_type_entry = str(purchase_type_list[i])
        
            name_entry = str(name_list[i])
        
            quantity_entry = str(quantity_list[i])
        
            if not purchase_type_entry == None:
                purchase_type = purchase_type_entry.split('>')[1].split('<')[0] 
            if not name_entry == None:
                name = name_entry.split('>')[1].split('<')[0]
                date = name_entry.split('"')[1]
            if not quantity_entry == None:
                quantity = quantity_entry.split('>')[1].split('<')[0]
        
            dataframe.loc[i] = ({'purchase_type':purchase_type,'name':name,'date':date,'quantity':quantity})
     
        return dataframe
       
    

### REPORTSNAPSHOT
    

    elif report_type =='ReportSnapshot':
        fundamental_data = app.reqFundamentalData(contract = contract, reportType = report_type 
        ,fundamentalDataOptions= [])
        soup = BeautifulSoup(fundamental_data,'xml')
    
        summary_list = soup.find_all('TextInfo')
        industry_info_list = soup.find_all('IndustryInfo')
        ratios_list = soup.find_all('Ratios')
        forecast_data_list = soup.find_all('ForecastData')
    

    #TODO: Make a dataframe with i= 1,2,...,n rows where each i is a stock with Business Summary and Industry Info 

        snapshot_financial_summary_dataframe = pd.DataFrame(columns = ['FinancialSummary'])
    
        for i in range(len(summary_list)):
            snapshot_financial_summary_entry = str(summary_list[i])

            if not snapshot_financial_summary_entry == None:
                snapshot_financial_summary = snapshot_financial_summary_entry.split('BRIEF:')[1].split('<')[0]

        snapshot_financial_summary_dataframe.loc[i] = ({'FinancialSummary':snapshot_financial_summary})

        ratios_dataframe = pd.DataFrame(columns = ['MKTCAP','TTMREV','TTMEBITD','TTMNIAC','TTMEPSXCLX','TTREVPS','QBVPS','QCSHPS','TTMCFSHR','TTMDIVSHR', 'TTMGROSMGN','TTMROEPCT','TTMPR2REV','PEEXCLXOR','PRICE2BK','EMPLOYEES'])

        for i in range(len(ratios_list)):
            ratios_list_entry = str(ratios_list[i])
           

            if not ratios_list_entry == None:
                #Income Statement
                MKTCAP = ratios_list_entry.split('Group ID=')[2].split('>')[2].split('<')[0]
                TTMREV = ratios_list_entry.split('Group ID=')[2].split('>')[4].split('<')[0]
                TTMEBITD = ratios_list_entry.split('Group ID=')[2].split('>')[6].split('<')[0]
                TTMNIAC = ratios_list_entry.split('Group ID=')[2].split('>')[8].split('<')[0]

                #per share data 
                TTMEPSXCLX = ratios_list_entry.split('Group ID=')[3].split('>')[2].split('<')[0]
                TTREVPS = ratios_list_entry.split('Group ID=')[3].split('>')[4].split('<')[0]
                QBVPS = ratios_list_entry.split('Group ID=')[3].split('>')[6].split('<')[0]
                QCSHPS = ratios_list_entry.split('Group ID=')[3].split('>')[8].split('<')[0]
                TTMCFSHR = ratios_list_entry.split('Group ID=')[3].split('>')[10].split('<')[0]    
                TTMDIVSHR = ratios_list_entry.split('Group ID=')[3].split('>')[12].split('<')[0]

                #other ratios

                TTMGROSMGN = ratios_list_entry.split('Group ID=')[4].split('>')[2].split('<')[0]
                TTMROEPCT = ratios_list_entry.split('Group ID=')[4].split('>')[4].split('<')[0]
                TTMPR2REV = ratios_list_entry.split('Group ID=')[4].split('>')[6].split('<')[0]
                PEEXCLXOR = ratios_list_entry.split('Group ID=')[4].split('>')[8].split('<')[0]
                PRICE2BK = ratios_list_entry.split('Group ID=')[4].split('>')[10].split('<')[0]
                EMPLOYEES = ratios_list_entry.split('Group ID=')[4].split('>')[12].split('<')[0]

        ratios_dataframe.loc[i] = ({'MKTCAP': MKTCAP, 'TTMREV': TTMREV, 'TTMEBITD': TTMEBITD, 'TTMNIAC': TTMNIAC, 'TTMEPSXCLX': TTMEPSXCLX ,'TTREVPS': TTREVPS,'QBVPS': QBVPS,'QCSHPS': QCSHPS,'TTMCFSHR':TTMCFSHR,'TTMDIVSHR':TTMDIVSHR, 'TTMGROSMGN':TTMGROSMGN,'TTMROEPCT':TTMROEPCT, 'TTMPR2REV': TTMPR2REV,'PEEXCLXOR': PEEXCLXOR,'PRICE2BK': PRICE2BK,'EMPLOYEES': EMPLOYEES})
       


        forecast_dataframe = pd.DataFrame(columns = ['ConRecom', 'TargetPrice', 'ProjLTGrowthRate', 'ProjPE', 'ProjSales',
            'ProjSalesQ','ProjEPS','ProjEPSQ','ProjProfit','ProjDPS'])

         
        for i in range(len(forecast_data_list)):
            forecast_data_list_entry = str(forecast_data_list[i])
        
            if not forecast_data_list_entry == None:
                ConRecom = ratios_list_entry.split('>')[3].split('<')[0]
                TargetPrice = ratios_list_entry.split('>')[7].split('<')[0]
                ProjLTGrowthRate = ratios_list_entry.split('>')[11].split('<')[0]
                ProjPE = ratios_list_entry.split('>')[15].split('<')[0] 
                ProjSales =  ratios_list_entry.split('>')[19].split('<')[0]
                ProjSalesQ =  ratios_list_entry.split('>')[23].split('<')[0]
                ProjEPS =  ratios_list_entry.split('>')[27].split('<')[0]
                ProjEPSQ =  ratios_list_entry.split('>')[31].split('<')[0]
                ProjProfit =  ratios_list_entry.split('>')[35].split('<')[0]
                ProjDPS =  ratios_list_entry.split('>')[39].split('<')[0]


        forecast_dataframe.loc[i] = ({'ConRecom': ConRecom, 'TargetPrice': TargetPrice, 'ProjLTGrowthRate': ProjLTGrowthRate, 'ProjPE': ProjPE, 'ProjSales': ProjSales,'ProjSalesQ': ProjSalesQ,'ProjEPS':ProjEPS,'ProjEPSQ':ProjEPSQ,'ProjProfit':ProjProfit,'ProjDPS':ProjDPS})
       
        return ratios_dataframe, forecast_dataframe

### ReportsFinStatements

    elif report_type == 'ReportsFinStatements':
        fundamental_data = app.reqFundamentalData(contract = contract, reportType = report_type 
        ,fundamentalDataOptions= [])
        soup = BeautifulSoup(fundamental_data,'xml')
    

        statement_line_item_list = soup.find_all('FiscalPeriod')
        

        income_dataframe = pd.DataFrame(columns = ['Statement_Date','SREV', 'RTLR', 'SCOR', 'SGRP', 'SSGA', 'ERAD', 'SUIE', 
        'ETOE', 'SOPI','SNIN','SONT','EIBT','TTAX','TIAT','CMIN','NIBX','NINC','CIAC','XNIC','SDNI','SDWS','SDBF','VDES'])
        
        balance_dataframe = pd.DataFrame(columns = ['Statement_Date','ACAE','SCSI', 'AACR','ARTC','AITL','APPY','SOCA','ATCA'
        ,'APTC','ADEP','APPN','AGWI','AINT','ALTR','SOLA','ATOT','LAPB','LAEX','LSTD','LCLD','SOCL','LTCL','LLTD','LCLO',
        'LTTD','STLD','LMIN','SLTL','LTLL','SCMS','QIPC','QRED','SOTE','QTLE','QTEL','QTCO','STBP'])

        cas_dataframe = pd.DataFrame(columns = ['Statement_Date','ONET','SDED','SNCI','SCTP','SCIP','SOCF','OTLO','SCEX',
        'SICF','ITLI','SFCF', 'FPSS','FPRD','FTLF', 'SFEE', 'SNCC'])
        

        for i in range(len(statement_line_item_list)):
           #statement_date_entry = str(statement_date_list[i]) 
            statement_line_item_entry = str(statement_line_item_list[i])

            if not statement_line_item_entry == None:
                
                Statement_Date = statement_line_item_entry.split('Source Date="')[1].split('"')[0]

                #income statement stuff
                SREV = statement_line_item_entry.split('>')[22].split('<')[0]
                RTLR = statement_line_item_entry.split('>')[24].split('<')[0]
                SCOR = statement_line_item_entry.split('>')[26].split('<')[0]
                SGRP = statement_line_item_entry.split('>')[28].split('<')[0]
                SSGA = statement_line_item_entry.split('>')[30].split('<')[0]
                ERAD = statement_line_item_entry.split('>')[32].split('<')[0]
                SUIE = statement_line_item_entry.split('>')[34].split('<')[0]      
                ETOE = statement_line_item_entry.split('>')[36].split('<')[0]
                SOPI = statement_line_item_entry.split('>')[38].split('<')[0]
                SNIN = statement_line_item_entry.split('>')[40].split('<')[0]
                SONT = statement_line_item_entry.split('>')[42].split('<')[0]
                EIBT = statement_line_item_entry.split('>')[44].split('<')[0]
                TTAX = statement_line_item_entry.split('>')[46].split('<')[0]
                TIAT = statement_line_item_entry.split('>')[48].split('<')[0]
                CMIN = statement_line_item_entry.split('>')[50].split('<')[0]
                NIBX = statement_line_item_entry.split('>')[52].split('<')[0]
                NINC = statement_line_item_entry.split('>')[54].split('<')[0]
                CIAC = statement_line_item_entry.split('>')[56].split('<')[0]
                XNIC = statement_line_item_entry.split('>')[58].split('<')[0]
                SDNI = statement_line_item_entry.split('>')[60].split('<')[0]
                SDWS = statement_line_item_entry.split('>')[62].split('<')[0]
                SDBF = statement_line_item_entry.split('>')[64].split('<')[0]
                VDES = statement_line_item_entry.split('>')[66].split('<')[0]
                
                #balance sheet stuff
                ACAE = statement_line_item_entry.split('>')[82].split('<')[0]
                SCSI = statement_line_item_entry.split('>')[84].split('<')[0]
                AACR = statement_line_item_entry.split('>')[86].split('<')[0]
                ARTC = statement_line_item_entry.split('>')[88].split('<')[0]
                AITL = statement_line_item_entry.split('>')[90].split('<')[0]
                APPY = statement_line_item_entry.split('>')[92].split('<')[0]
                SOCA = statement_line_item_entry.split('>')[94].split('<')[0]
                ATCA = statement_line_item_entry.split('>')[96].split('<')[0]
                APTC = statement_line_item_entry.split('>')[98].split('<')[0]
                ADEP = statement_line_item_entry.split('>')[100].split('<')[0]
                APPN = statement_line_item_entry.split('>')[102].split('<')[0]
                AGWI = statement_line_item_entry.split('>')[104].split('<')[0]
                AINT = statement_line_item_entry.split('>')[106].split('<')[0]
                ALTR = statement_line_item_entry.split('>')[108].split('<')[0]
                SOLA = statement_line_item_entry.split('>')[110].split('<')[0]
                ATOT = statement_line_item_entry.split('>')[112].split('<')[0]
                LAPB = statement_line_item_entry.split('>')[114].split('<')[0]
                LAEX = statement_line_item_entry.split('>')[116].split('<')[0]
                LSTD = statement_line_item_entry.split('>')[118].split('<')[0]
                LCLD = statement_line_item_entry.split('>')[120].split('<')[0] 
                SOCL = statement_line_item_entry.split('>')[122].split('<')[0]
                LTCL = statement_line_item_entry.split('>')[124].split('<')[0]
                LLTD = statement_line_item_entry.split('>')[126].split('<')[0]
                LCLO = statement_line_item_entry.split('>')[128].split('<')[0]
                LTTD = statement_line_item_entry.split('>')[130].split('<')[0] 
                STLD = statement_line_item_entry.split('>')[132].split('<')[0]
                LMIN = statement_line_item_entry.split('>')[134].split('<')[0]
                SLTL = statement_line_item_entry.split('>')[136].split('<')[0]
                LTLL = statement_line_item_entry.split('>')[138].split('<')[0]
                SCMS = statement_line_item_entry.split('>')[140].split('<')[0]
                QIPC = statement_line_item_entry.split('>')[142].split('<')[0]
                QRED = statement_line_item_entry.split('>')[144].split('<')[0]
                SOTE = statement_line_item_entry.split('>')[146].split('<')[0]
                QTLE = statement_line_item_entry.split('>')[148].split('<')[0]
                QTEL = statement_line_item_entry.split('>')[150].split('<')[0]
                QTCO = statement_line_item_entry.split('>')[152].split('<')[0]
                STBP = statement_line_item_entry.split('>')[154].split('<')[0]

                #cas stuff
                ONET = statement_line_item_entry.split('>')[174].split('<')[0]
                SDED = statement_line_item_entry.split('>')[176].split('<')[0]
                SNCI = statement_line_item_entry.split('>')[178].split('<')[0]
                SCTP = statement_line_item_entry.split('>')[180].split('<')[0]
                SCIP = statement_line_item_entry.split('>')[182].split('<')[0]
                SOCF = statement_line_item_entry.split('>')[184].split('<')[0]
                OTLO = statement_line_item_entry.split('>')[186].split('<')[0]
                SCEX = statement_line_item_entry.split('>')[188].split('<')[0]
                SICF = statement_line_item_entry.split('>')[190].split('<')[0]
                ITLI = statement_line_item_entry.split('>')[192].split('<')[0]
                SFCF = statement_line_item_entry.split('>')[194].split('<')[0]
                FPSS = statement_line_item_entry.split('>')[196].split('<')[0]
                FPRD = statement_line_item_entry.split('>')[198].split('<')[0]
                FTLF = statement_line_item_entry.split('>')[200].split('<')[0]
                SFEE = statement_line_item_entry.split('>')[202].split('<')[0]
                SNCC = statement_line_item_entry.split('>')[204].split('<')[0]
        
                income_dataframe.loc[i] = ({'Statement_Date': Statement_Date,'SREV': SREV, 'RTLR' :RTLR, 'SCOR':SCOR, 'SGRP':SGRP, 'SSGA': SSGA, 'ERAD':ERAD, 'SUIE':SUIE, 
                'ETOE':ETOE, 'SOPI':SOPI,'SNIN':SNIN,'SONT':SONT,'EIBT':EIBT,'TTAX':TTAX,'TIAT':TIAT,'CMIN':CMIN,'NIBX':NIBX,'NINC':NINC,'CIAC':CIAC,'XNIC':XNIC,'SDNI':SDNI,
                'SDWS':SDWS,'SDBF':SDBF,'VDES':VDES})
        
                balance_dataframe.loc[i] = ({'Statement_Date':Statement_Date,'ACAE':ACAE,'SCSI':SCSI, 'AACR':AACR,'ARTC':ARTC,'AITL':AITL,'APPY':APPY,'SOCA':SOCA,'ATCA':ATCA
                ,'APTC':APTC,'ADEP':ADEP,'APPN':APPN,'AGWI':AGWI,'AINT':AINT,'ALTR':ALTR,'SOLA':SOLA,'ATOT':ATOT,'LAPB':LAPB,'LAEX':LAEX,'LSTD':LSTD,'LCLD':LCLD,'SOCL':SOCL,'LTCL':LTCL,'LLTD':LLTD,'LCLO':LCLO,
                'LTTD':LTTD,'STLD':STLD,'LMIN':LMIN,'SLTL':SLTL,'LTLL':LTLL,'SCMS':SCMS,'QIPC':QIPC,'QRED':QRED,'SOTE':SOTE,'QTLE':QTLE,'QTEL':QTEL,'QTCO':QTCO,'STBP':STBP})

                cas_dataframe.loc[i] = ({'Statement_Date':Statement_Date,'ONET':ONET,'SDED':SDED,'SNCI':SNCI,'SCTP':SCTP,'SCIP':SCIP,'SOCF':SOCF,'OTLO':OTLO,'SCEX':SCEX,
                'SICF':SICF,'ITLI':ITLI,'SFCF':SFCF, 'FPSS':FPSS,'FPRD':FPRD,'FTLF':FTLF, 'SFEE':SFEE, 'SNCC':SNCC})

        return income_dataframe, balance_dataframe, cas_dataframe
            
                
 
#RESC (Analyst Estimates)
    elif report_type == 'RESC':
        #Choose between Actual and Expected Earnings

        if analyst_type == 'Actual':
            fundamental_data = app.reqFundamentalData(contract = contract, reportType = report_type 
            ,fundamentalDataOptions= [])
            fundamental_data = fundamental_data.split('ConsEstimates')[0]
            soup = BeautifulSoup(fundamental_data,'xml')
            
            #to grab actual_type, unit, periodType, fYear, endMonth, endCalYear, ActValue
            actual_list_1 = soup.find_all('FYActual')
            print(len(actual_list_1))
            
            #to grab periodType, fYear, endMonth, endCalYear, ActValue
            actual_list_2 = soup.find_all('FYPeriod')
            print(len(actual_list_2))
            
            #creating index array by using the length of the longer list
            index_actual = []
            if len(actual_list_2) >  len(actual_list_1):
                for i in range(len(actual_list_2)):
                    index_actual.append(i)
            else: 
                for i in range(len(actual_list_1)):
                    index_actual.append(i)
            
            print(index_actual)
        
            #creating dataframe

            actual_dataframe_1 = pd.DataFrame(columns = ['actual_type','unit']) 
            actual_dataframe_2 = pd.DataFrame(columns = ['periodNum', 'periodType', 'fyear', 'endMonth', 'endCalYear', 'updated' 'ActValue'])
            print(actual_dataframe_2)

            
            for i in range(len(actual_list_1)):
                actual_entry_1 = str(actual_list_1[i])
                if not actual_entry_1 == None:
                    actual_type = actual_entry_1.split('"')[1].split('"')[0]
                    unit = actual_entry_1.split('"')[3].split('"')[0]
                    actual_dataframe_1.loc[i] = ({'actual_type':actual_type, 'unit':unit})
            print(actual_dataframe_1)
            for i in range(len(index_actual)):
                actual_entry_2 = str(actual_list_2[i])
                print(actual_entry_2)
            ''' 
                if not actual_entry_2 == None:            

                    periodNum = actual_entry_1.split('periodNum="')[1].split('"')[0]
                    periodType = actual_entry_1.split('periodType="')[1].split('"')[0]
                    fyear = actual_entry_1.split('fYear="')[1].split('"')[0]
                    endMonth = actual_entry_1.split('endMonth="')[1].split('"')[0]
                    endCalYear = actual_entry_1.split('endCalYear="')[1].split('"')[0]
                    ActValue = actual_entry_1.split('">')[3].split('<')[0]


                
                    actual_dataframe_2.loc[i] = ({'periodNum': periodNum, 'periodType': periodType, 'fyear': fyear, 'endMonth':endMonth, 'endCalYear':endCalYear, 'ActValue':ActValue})
            print(actual_dataframe_2)

            '''
            for i in range(len(actual_list_2)):
                actual_entry_2 = str(actual_list_2[i])
                print(actual_entry_2)
                if not actual_entry_2 == None:
                
                    periodNum = actual_entry_2.split('periodNum="')[1].split('"')[0]
                    periodType = actual_entry_2.split('periodType="')[1].split('"')[0]
                    fyear = actual_entry_2.split('fYear="')[1].split('"')[0]
                    endMonth = actual_entry_2.split('endMonth="')[1].split('"')[0]
                    endCalYear = actual_entry_2.split('endCalYear="')[1].split('"')[0]
                    ActValue = actual_entry_2.split('>')[2].split('<')[0]
                
                
                    actual_dataframe_2.loc[i] = ({'periodNum': periodNum, 'periodType': periodType, 'fyear': fyear, 'endMonth':endMonth, 'endCalYear':endCalYear, 'ActValue':ActValue})
            print(actual_dataframe_2)
            #dataframe = pd.concat(obs = [actual_dataframe_1, actual_dataframe_2], axis = 1)
            #print(dataframe)

        '''
        elif analyst_type == 'Estimate':

            fundamental_data = app.reqFundamentalData(contract = contract, reportType = report_type 
            ,fundamentalDataOptions= [])
            fundamental_data = fundamental_data.split('ConsEstimates')[1]
            soup = BeautifulSoup(fundamental_data,'xml')

            estimate_dataframe = pd.DataFrame(columns = ['estimate_type', 'unit', 'periodNum', 'periodType', 'fYear', 'endMonth', 'endCalYear', 'date_type','estimate'])

            #to grab estimate_type and unit
            estimate_list_1 = str(soup.find_all('FYEstimate'))

            #to grab periodtype, fYear, endMonth, endCalYear
            pre_list = str(soup.find_all('FYPeriod'))
            estimate_list_2 = pre_list.split('<ConsEstimates>')[1]
            

            #to grab estimate_type
            estimate_list_3 = str(soup.find_all('ConsEstimate'))


            # to grab date_type, value
            estimate_list_4 = str(soup.find_all('ConsValue'))



            for i in range(len(actual_list)):
                actual_entry = str(actual_list[i])
                if not actual_entry == None:
                    

                    actual_type = actual_entry.split('"')[1].split('"')[0]
                    actual_unit = actual_entry.split('"')[3].split('"')[0]
                    endCalYear = actual_entry.split('"')[5].split('"')[0]
                    endMonth = actual_entry.split('"')[7].split('"')[0]
                    fyear = actual_entry.split('"')[9].split('"')[0]
                    periodType = actual_entry.split('"')[11].split('"')[0]
                    ActValue = actual_entry.split('>')[1].split('"')[0]
                
                
                    #actual_dataframe.loc[i] = ({'endcalYear': endCalYear, 'endMonth': endMonth, 'periodNum': periodNum, 'periodType':periodType, 'type':actual_type,
                    #'ActValue':ActValue})
       
            '''
        

        

        



    elif report_type == None and news == True:
        # grabbing conid and providers
        contract_details = str(app.reqContractDetails(contract))
        conid = contract_details.split('=')[3].split(',')[0]
        print(conid)
        
        #TODO: figure out if this will work
        newsProviders = app.reqNewsProviders()
        print(newsProviders)
        codes = '+'.join(np.code for np in newsProviders)
        print(codes)   

        headlines = app.reqHistoricalNews(conid, codes, startDateTime= datetime.datetime.now, endDateTime ='', totalResults= 10)
        latest = headlines[0]
        print(latest)
        article = app.reqNewsArticle(latest.providerCode, latest.articleId)
        print(article)
       

def main():
    grab_contract(asset_class = 'Stock', ticker = 'AAPL', 
    option_type = None, option_strike = None, option_expiry = None)

    parseXML(data = fundamental_data, report_type = 'RESC', analyst_type = 'Actual', news = False)

if __name__ == "__main__":
    main()

    '''
Report Types:
-ReportsFinSummary
    -asofDate
    -period
    -reportType
    -Total Revenue
    -EPS


-ReportsOwnership
    - purchase_type
    - name
    - quantity
    
-ReportSnapshot
    - Business Summary
    - IndustryInfonilu

    - Financial Summary

    - Ratios 
        - Income Statement
            - MKTCAP
            - TTMREV
            - TTMEBITD
            - TTMNIAC
        - Per Share Data
            - TTMEPSXCLX
            - TTMREVPS
            - QBVPS
            - QCSHPS
            - TTMCFSHR
            - TTMDIVSHR
        - Other Ratios
            - TTMGROSMGN
            - TTMROEPCT
            - TTMPR2REV
            - PEEXCLXOR
            - PRICE2BK
            - EMPLOYEES

    - ForecastData
        - ConsRecom
        - TargetPrice
        - ProjLTGrowthRate
        - ProjPE
        - ProjSales
        - ProjSalesQ
        - ProjEPS
        - ProjEPSQ
        - ProjProfit
        - ProjDPS




-ReportsFinStatements
    -Statement Date

# Income Statement

    -SREV (Revenue)
    -RTLR (Total Revenue)
    -SCOR (Cost of Revenue)
    -SGRP (Gross Profit)
    -SSGA (Selling/General/Admin. Expenses, Total)
    -ERAD (Research & Development)
    -SUIE (Unusual Expense (Income))
    -ETOE 
    -SOPI (Operating Income)
    -SNIN (Interest Inc.(Exp.), Net-non-Op.,Total)
    -SONT (Other, Net)
    -EIBT (Net Income Before Taxes)
    -TTAX (Provision for Income Taxes)
    -TIAT (Net Income After Taxes)
    -CMIN (Minority Interest)
    -NIBX (Net Income Before Extra. Items)
    -NINC (Net Income)
    -CIAC (Income Available to Com Excl ExtraOrd)
    -XNIC (Income Available to Com Incl ExtraOrd)
    -SDNI (Diluted Net Income)
    -SDWS (Diluted Weighted Average Sharews)
    -SDBF (Diluted EPS Excluding ExtraOrd Items)
    -VDES (Diluted Normalized EPS)

# Balance Sheet

    -ACAE (Cash & Equivalents)
    -SCSI (Cash & Short Term Investments)
    -AACR (Accounts Recievable - Trade, Net)
    -ATRC (Total Receivables, Net)
    -AITL (Total Inventory)
    -APPY (Prepaid Expenses)
    -SOCA (Other Current Assets)
    -ATCA (Total Current Assets)
    -APTC (Property/Plant/Equipment, Total- Gross)
    -ADEP (Accumulated Depreciation, Total)
    -APPN (Property/Plant/Equipment, Total- Net)
    -AGWI (Goodwill, Net)
    -AINT (Intangibles, Net)
    -ALTR (Note Receivable - Long Term)
    -SOLA (Other Long Term Assets, Total)
    -ATOT (Total Assets)
    -LAPB (Accounts Payable)
    -LAEX (Accrued Expenses)
    -LSTD (Notes Payable/Short Term Debt)
    -LCLD (Current Port. of LT Debt/Capital Leases)
    -SOCL (Other Current Liabilities, Total)
    -LTCL (Total Current Liabilities)
    -LLTD (Long Term Debt)
    -LCLO (Capital Lease Obligations)
    -LTTD (Total Long Term Debt)
    -STLD (Total Debt)
    -LMIN (Minority Interest)
    -SLTL (Other Liabilities)
    -LTLL (Total Liabilities)
    -SCMS (Common Stock, Total)
    -QPIC (Additional Paid-In Capital)
    -QRED (Retained Earnings (Accumulated Deficit))
    -SOTE (Other Equity)
    -QTLE (Total Equity)
    -QTEL (Total Liabilities & Shareholders Equity)
    -QTCO (Total Common Shares Outstanding)
    -STBP (Tangible Book Value per Share)

# Cost Accounting Standards

    -ONET (Net Income/Starting Line)
    -SDED (Depreciation/Depletion)
    -SNCI (Non-Cash Items)
    -SCTP (Taxes Paid)
    -SCIP (Cash Interest Paid)
    -SOCF (Changes in Working Capital)
    -OTLO (Cash from Operating Activities)
    -SCEX (Capital Expenditures)
    -SICF (Other Investing Cash Flow Items, Total)
    -ITLI (Cash from Investing Activities)
    -SFCF (Financing Cash Flow Items)
    -FPSS (Issuance (Retirement) of Stock, Net)
    -FPRD (Issuance (Retirement) of Debt, Net)
    -FTLF (Cash from Financing Activities)
    -SFEE (Foreign Exchange Effects)
    -SNCC (Net Change in Cash)


   
-RESC (analyst estimates)
    -REarnEstCons
        -Company
        -Actuals
        -ConsEstimates


-CalenderReport (can't get without subscription)
    

    '''

