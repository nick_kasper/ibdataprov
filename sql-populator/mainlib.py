#%%
from ib_insync import *

import threading
import time
from dateutil.relativedelta import relativedelta

from IPython.display import display, clear_output

import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt

import mlfinlab as ml
import pyodbc

import xml.etree.cElementTree as ET



class Data_Provider(object):
    contract = 'contract'
    live_data = []
    research_data = []
    fundamental_data = ''
    headlines = ''
    data_arrival = 'research'

    def __init__(self, data_arrival, asset_class, ticker, option_type, option_strike, option_expiry, bar_method_retrieve,
    live_tick_type, live_time_bars_what_to_show, research_data_type, time_bar_length, time_bar_duration, fundamental_report_type,
    tick_bar_start, tick_bar_end, historical_tick_bars_what_to_show, reqMkt_desired_ticks, req_mkt_type):
           
           '''

           First goes to grab_contract to grab the contract object
           Then goes to either retrieve_live or retrieve_research and grabs the proper data
           
           Appropriate data is then routed to data_transform which uses AFML methods to transform the data

           Finally, it outputs the data object in either csv, pandas df, or to sql databases

           Parameters:
        
           data_arrival(string): either 'live' or 'research' functionality


           asset_class(string): 'stock', 'option', 'forex', or NONE if non market data

           ticker (string): if stock or forex, pass in ticker (ex 'AAPL'/'EURUSD'). If option, simply pass in the underlying stock. 

           option_type(string): 'call', 'put' or NONE if non option

           option_strike(int): 240 strike price would be 240 ' NONE if non option

           option_expiry(string): 'yyyymmdd' ex: '20170721' NONE if non option



           bar_method_retrieve (string): 'time' or 'tick' for the retrieve methods

           live_tick_type (string): Type of tick. One of ‘Last’, ‘AllLast’, ‘BidAsk’ or ‘MidPoint’.

           live_number_of_ticks (int): Number of ticks or 0 for unlimited #currently not needed

           live_time_bars_what_to_show (str): Specifies the source for constructing bars.  
          Can be 'TRADES', 'MIDPOINT', 'BID' or 'ASK'. 


           research_data_type(string): 'market' for price, 'market_alt' for things like historical vol, open interest, volume,etc, 'fundamental' for analyst reports and statements, 'news' for news fundamental data, more categories to be added
        
           time_bar_length (string): only used for time bars. Choose the length of one row in your dataset
                '1 secs', '5 secs', '10 secs', 15 secs', '30 secs',
                '1 min', '2 mins', '3 mins', '5 mins', '10 mins', '15 mins', '20 mins', '30 mins',
                '1 hour', '2 hours', '3 hours', '4 hours', '8 hours',
                '1 day', '1 week', '1 month'.
            
           time_bar_duration (string): Only use if time bar. Time span of all the time bars. Examples: 
           '60 S', '30 D', '13 W', '6 M', '10 Y'.
 
           tick_bar_start(string): Can be given as a datetime.date or datetime.datetime, 
           or it can be given as a string in ‘yyyyMMdd HH:mm:ss’ format. If no timezone is given then the TWS login timezone is used.

           tick_bar_end(string):  One of tick_bar_start or tick_bar_end can be given, the other must be blank.




           bar_method_transform (string):  Choose method based on chapter 2 of AFML
           'tick', 'dollar', 'volume', 'time', 'ema_dollar_imbalance', 'ema_volume_imbalance',
           'ema_tick_imbalance', 'const_dollar_imbalance', 'const_volume_imbalance', 'const_tick_imbalance', 
           'ema_dollar_run', 'ema_volume_run', 'ema_tick_run', 'const_dollar_run', 'const_volume_run', 'const_tick_run'

           

           output_file (string): input 'data_frame' for pandas dataframe, 'csv' for csv, 'sql' for sql

        '''

    
    
    data_arrival = 'research'
    asset_class = 'Stock'
    ticker = 'AAPL'
    option_type = None
    option_strike = None
    bar_method_retrieve = 'time'
    live_tick_type = None
    live_time_bars_what_to_show = None
    research_data_type = 'market'
    time_bar_length = None
    time_bar_duration = '1 Y'
    fundamental_report_type = None
    tick_bar_start = None
    tick_bar_end = None
    historical_tick_bars_what_to_show = None
    reqMkt_desired_ticks = None
    

       


    def retrieve_live(self, bar_method_retrieve, live_tick_type, live_time_bars_what_to_show):

        '''
        Retrieves live data:

        reqTickbyTickData for tick data (can only do 3 at a time when 2 are reserved for bid/ask and time/sales)
        realTimeBars for realtime data (must be 5 second bars)

        Parameters:

        bar_method_retrieve (string): 'time' or 'tick'
    
        live_tick_type (string): Type of tick. One of ‘Last’, ‘AllLast’, ‘BidAsk’ or ‘MidPoint’.

        live_number_of_ticks (int): Number of ticks or 0 for unlimited ##currently not needed

        live_time_bars_what_to_show (str): Specifies the source for constructing bars.  
          Can be 'TRADES', 'MIDPOINT', 'BID' or 'ASK'.  'NONE' if not time bars

       '''

        #Connecting...

        app = IB()
        app.connect('127.0.0.1', 4001, clientId = 1, timeout = 5)
        
        if app.client.isReady() == True:
            print("We're good to go!")
        else:
            print("API is having issues connecting!")
            app.disconnect()
        
        #Grab Live Data


        if bar_method_retrieve == 'tick':
            live_data = app.reqTickByTickData(contract = contract, tickType = live_tick_type)
            return live_data.tickByTicks
        elif bar_method_retrieve == 'time':
            live_data = app.reqRealTimeBars(contract = contract, barsize = 5, whatToShow = live_time_bars_what_to_show,
            useRTH = True, realTimeBarsOptions= [])
            return live_data
        else:
            print ('Error grabbing live data!')

    

      
    
    def data_transform(self, bar_method_transform, start_date, end_date, output_file):

        '''
        Transforms data from retrieve method into final form via mlfinlab methods, returned via pandas dataframes, csv, sql database, txt 

        Parameters:

        bar_method_transform (string):  Choose method based on chapter 2 of AFML
           'tick', 'dollar', 'volume', 'time', 'ema_dollar_imbalance', 'ema_volume_imbalance',
           'ema_tick_imbalance', 'const_dollar_imbalance', 'const_volume_imbalance', 'const_tick_imbalance', 
           'ema_dollar_run', 'ema_volume_run', 'ema_tick_run', 'const_dollar_run', 'const_volume_run', 'const_tick_run'


        start_date (string): 

        end_date (string):

        output_file (string): input 'data_frame' for pandas dataframe, 'csv' for csv, 'sql' for sql
        '''
        


    
if __name__ == '__main__':
    # database cursor
    #global db_cursor
    # connect to database 
    #cnx = mysql.connector.connect(user='root', password='{dwrKYAKQM5tts%F*S&9HB^S}', host = '127.0.0.1', database='', auth_plugin='mysql_native_password')

    #db_cursor = cnx.cursor(buffered=True) 


    dp = Data_Provider(data_arrival = 'research', asset_class = 'stock', ticker = 'AAPL', 
    option_type = None, option_strike = None, option_expiry = None, bar_method_retrieve = 'time', 
    live_tick_type = None , live_time_bars_what_to_show = None , research_data_type = 'market', 
    time_bar_length = None, time_bar_duration = '1 Y', fundamental_report_type = None, tick_bar_start = None, 
    tick_bar_end = None, historical_tick_bars_what_to_show = None, reqMkt_desired_ticks= None,req_mkt_type = 3)  

    dp.grab_contract(asset_class = 'Stock', ticker = 'TSLA', 
    option_type = None, option_strike = None, option_expiry = None)

    global data_arrival

    data_arrival = 'research'

    if data_arrival == 'research':

        dp.retrieve_research(research_data_type = 'fundamental', bar_method_retrieve = 'tick',
        time_bar_length = None , time_bar_duration = None , fundamental_report_type = 'ReportsFinStatements', tick_bar_start = None, 
        tick_bar_end = None, historical_tick_bars_what_to_show = 'Midpoint' , reqMkt_desired_ticks= 221, req_mkt_type = 4)

    else:
        dp.retrieve_live(bar_method_retrieve = 'time', live_tick_type = None , live_time_bars_what_to_show = None)




# %%
def retrieve_research(self, research_data_type, bar_method_retrieve, time_bar_length, time_bar_duration, fundamental_report_type, tick_bar_start,
tick_bar_end, historical_tick_bars_what_to_show,reqMkt_desired_ticks,req_mkt_type):

    '''
    Connects to IB and retrieves specific data via IB specified by parameters:

    Parameters:
    
    research_data_type(string): 'market' for price, volume,etc. 'fundamental' for fundamental data, 'news' for news . more categories to be added
    'market_alt' for access to reqMktData functionality (putvolume,callvolume,hist volatility, etc)

   
    bar_method_retrieve (string): choose between 'tick' and 'time' or NONE if non market data

    time_bar_length (string): only used for time bars. Choose the length of one row in your dataset
            '1 secs', '5 secs', '10 secs', 15 secs', '30 secs',
            '1 min', '2 mins', '3 mins', '5 mins', '10 mins', '15 mins', '20 mins', '30 mins',
            '1 hour', '2 hours', '3 hours', '4 hours', '8 hours',
            '1 day', '1 week', '1 month'.

    time_bar_duration (string): Only use if time bar. Time span of all the time bars. Examples: 
       '60 S', '30 D', '13 W', '6 M', '10 Y'.

    tick_bar_start(string): Can be given as a datetime.date or datetime.datetime, 
    or it can be given as a string in ‘yyyyMMdd HH:mm:ss’ format. If no timezone is given then the TWS login timezone is used.

    tick_bar_end(string):  One of tick_bar_start or tick_bar_end can be given, the other must be blank.

    historical_tick_bars_what_to_show (str): Specifies the source for constructing bars.  
      Can be 'Bid_Ask', 'Midpoint', or 'Trades'.  'NONE' if not historical tick bars

    reqMkt_desired_ticks (str): 
    genericTickList (str) : Comma seperated IDs (ints) of desired ticks. This data will fill into Ticker.field. 
    90: Streaming level-1 ticks of type TickData are stored in the ticks list
    100 : putVolume, callVolume (fields) (ex call Ticker.putVolume to get the data) 
    101: putOpenInterest, callOpenInterest
    104: histVolatility (options)
    105: avOptionVolume (options)
    106: impliedVolatility (options)
    162: indexFuturePremium
    165: low13Week, high13week, low26week, high26week, low52week, high52week, avVolune
    221: markPrice
    225: auctionVolume, auctionPrice, auctionImbalance
    233: last, lastSize, rtVolume, vwap (Time &Sales)
    236: shortableShares
    258: fundamentalRatios 
    293: tradeCount
    294: tradeRate
    295: volumeRate
    375: rtTradeVolume
    411: rtHistVolatility
    456: dividends
    588: futuresOpenInterest

    fundamental_report_type (string): 
    '‘ReportsFinSummary’: Financial summary

    ’ReportsOwnership’: Company’s ownership

    ’ReportSnapshot’: Company’s financial overview

    ’ReportsFinStatements’: Financial Statements

    ’RESC’: Analyst Estimates

    ’CalendarReport’: Company’s calendar  #may not be working
    
    req_mkt_type (int):
    
    Set the market data type
    1= Live
    2= Frozen
    3= Delayed
    4= Delayed frozen

    '''
    global research_data
    global fundamental_data
    global headlines
    #connecting...  
    app = IB()
    app.connect('127.0.0.1', 7497, clientId = 1, timeout = 5)
    
    if app.client.isReady() == True:
        print("We're good to go!")
    else:
        print("API not connected!")
        app.disconnect()

    #Grabs news providers
    newsProviders = app.reqNewsProviders()
    codes = '+'.join(np.code for np in newsProviders)
    
    # Takes parameters research_data_type and bar_method_retrieve to select the correct historical/research data we will be grabbing

    datetime_now = datetime.datetime.now()
    
    if research_data_type == 'market' and bar_method_retrieve == 'tick':
        # Create dataframe to store all requested data
        historical_tick_data = pd.DataFrame(columns = ['time','tickAttribLast','price','size','exchange','specialConditions'])
        # assume first available date is 6 months ago 
        time = datetime_now + relativedelta(months=-6)
        print(type(time))
        print(time)
        # begin loop starting at beginning of time
        while not time == datetime_now: 
            # make request
            research_data = app.reqHistoricalTicks(contract = contract, startDateTime = time, endDateTime = '', numberOfTicks = 1000, whatToShow = historical_tick_bars_what_to_show, useRth = False) 

            research_data = util.df(research_data)
            print(research_data) 
            frames = [historical_tick_data, research_data]

            historical_tick_data = pd.concat(frames, sort=False)
            print(historical_tick_data)
            # add data to dataframe
            # increment time
            time = time + datetime.timedelta(seconds=1)
            print(time)

        print(time)
        print(historical_tick_data.head())

        
        research_data = app.reqHistoricalTicks(contract = contract, startDateTime = '20200810 02:34:32', endDateTime = '', numberOfTicks = 1000, whatToShow = historical_tick_bars_what_to_show, useRth = False) 
        print(type(research_data))
        dataframe = util.df(research_data)
        print(dataframe.head())
        print(dataframe.tail())
    elif research_data_type == 'market' and bar_method_retrieve == 'time':
        
        research_data = app.reqHistoricalData(contract = contract, endDateTime='',
        durationStr= time_bar_duration, barSizeSetting= time_bar_length, whatToShow='TRADES', useRTH=True, formatDate=1)
        dataframe = util.df(research_data)
        print(dataframe.head())
        print(dataframe.tail())
        dataframe.plot(y='close')
    elif research_data_type == 'market_alt':
        #TODO: Need to fix the ticker object storing of data 
        app.reqMarketDataType(marketDataType = 1)
        app.sleep(2)
        ticker_object = app.reqMktData(contract, genericTickList= reqMkt_desired_ticks, snapshot = False,
                regulatorySnapshot = False, mktDataOptions = [])
        print(ticker_object)
        now = datetime.datetime.now()
        app.sleep(2)
        if reqMkt_desired_ticks == 100:
            dataframe= pd.DataFrame(index = [now], columns = ['putVolume', 'callVolume'])
            dataframe.loc[now] = (ticker_object.putVolume, ticker_object.callVolume)
            clear_output(wait=True)
            print(dataframe)       
        elif reqMkt_desired_ticks == 101:
            dataframe= pd.DataFrame(index = [now], columns = ['putOpenInterest', 'callOpenInterest'])
            dataframe.loc[now] = (ticker_object.putOpenInterest, ticker_object.callOpenInterest)
            clear_output(wait=True)
            print(dataframe)
        elif reqMkt_desired_ticks == 104:
            dataframe = pd.DataFrame(index = [now], columns = ['histVolatility'])
            dataframe.loc[now] = (ticker_object.histVolatility)
            clear_output(wait=True)
            print(dataframe)
        elif reqMkt_desired_ticks == 105:
            dataframe = pd.DataFrame(index = [now], columns = ['avOptionVolume'])
            dataframe.loc[now] = (ticker_object.avOptionVolume)
            clear_output(wait=True)
            print(dataframe)
        elif reqMkt_desired_ticks == 106:
            dataframe = pd.DataFrame(index = [now], columns = ['impliedVolatility'])
            dataframe.loc[now] = (ticker_object.impliedVolatility)
            clear_output(wait=True)
            print(dataframe)
        elif reqMkt_desired_ticks == 162:
            dataframe = pd.DataFrame(index = [now], columns = ['indexFuturePremium'])
            dataframe.loc[now] = (ticker_object.indexFuturePremium)
            clear_output(wait=True)
            print(dataframe)
        elif reqMkt_desired_ticks == 165:
            dataframe = pd.DataFrame(index = [now], columns = ['low13week', 'high13week', 'low26week', 'high26week', 'low52week', 'high52week', 'avVolume'])
            dataframe.loc[now] = (ticker_object.low13week, ticker_object.high13week, ticker_object.low26week, ticker_object.high26week,
            ticker_object.low52week, ticker_object.high52week, ticker_object.avVolume)
            clear_output(wait=True)
            print(dataframe)
        elif reqMkt_desired_ticks == 221:
            dataframe = pd.DataFrame(index = [now], columns = ['markPrice'])
            dataframe.loc[now] = (ticker_object.markPrice)
            clear_output(wait=True)
            print(dataframe)
        elif reqMkt_desired_ticks == 225:
            dataframe = pd.DataFrame(index = [now], columns = ['auctionVolume', 'auctionPrice', 'auctionImbalance'])
            dataframe.loc[ticker_object.contract] = (ticker_object.auctionVolume, ticker_object.auctionPrice, ticker_object.auctionImbalance)
            clear_output(wait=True)
            print(dataframe)
        elif reqMkt_desired_ticks == 233:
            dataframe222 = pd.DataFrame(index = [now], columns = ['last', 'lastSize', 'rtVolume', 'vwap'])
            print(dataframe222)
            #dataframe222.loc[ticker_object.contract] = (ticker_object.last, ticker_object.lastSize, ticker_object.rtVolume,ticker_object.vwap)
        elif reqMkt_desired_ticks == 236:
            ticker_object.shortableShares
        elif reqMkt_desired_ticks == 258:
            ticker_object.fundamentalRatios
        elif reqMkt_desired_ticks == 293:
            ticker_object.tradeCount
        elif reqMkt_desired_ticks == 294:
            ticker_object.tradeRate
        elif reqMkt_desired_ticks == 295:
            ticker_object.volumeRate
        elif reqMkt_desired_ticks == 375:
            ticker_object.rtTradeVolume
        elif reqMkt_desired_ticks == 411:
            ticker_object.rtHistVolatility
        elif reqMkt_desired_ticks == 456:
            ticker_object.dividends
        elif reqMkt_desired_ticks == 588:
            ticker_object.futuresOpenInterest
        else:
            print('Data Not Found')

    elif research_data_type == 'fundamental':
        fundamental_data = app.reqFundamentalData(contract = contract, reportType = fundamental_report_type,
        fundamentalDataOptions= [])
        

            
             
    elif research_data_type == 'news':
        conid = app.underConId(contract)
        headlines = app.reqHistoricalNews(conId = conid, providerCodes=codes, startDateTime = '', endDateTime ='', totalResults = 300)
        latest = headlines[0]
        print(latest)
        article = app.reqNewsArticle(latest.providerCode, latest.articleId)
        print(article)
        return headlines
    else:
        print('There was an error in grabbing your research data!')

def grab_contract(asset_class, ticker, option_type, option_strike, option_expiry, app):
            
        '''
        Parameters:
        asset_class(string): 'stock', 'option', 'forex', or NONE if non market data
        Will add CFDs, Bonds in the future

        ticker(string): if stock or forex, pass in ticker (ex 'AAPL'/'EURUSD'). 
        If option, simply pass in the underlying stock. 

        option_type(string): 'call', 'put' or NONE if non option

        option_strike(int): 240 strike price would be 240 NONE if non option

        option_expiry(string): 'yyyymmdd' ex: '20170721' NONE if non option



        Ways the contract object can be defined
        Contract(conId=270639)
        Stock('AMD', 'SMART', 'USD')
        Stock('INTC', 'SMART', 'USD', primaryExchange='NASDAQ')
        Forex('EURUSD')
        CFD('IBUS30')
        Future('ES', '20180921', 'GLOBEX')
        Option('SPY', '20170721', 240, 'C', 'SMART')
        Bond(secIdType='ISIN', secId='US03076KAA60')

        '''

            

        #TODO: Add some way of being able to store multiple contracts so if someone wants a certain set of assets
        # they could grab all of them 
        
    
        global contract 

        if asset_class == 'Stock':
            contract = Stock(ticker, 'SMART', 'USD')
        elif asset_class == 'option' and option_type == 'call':
            contract = Option(ticker, option_expiry, option_strike , 'C', 'SMART')
        elif asset_class == 'option' and option_type == 'put':
            contract = Option(ticker, option_expiry, option_strike , 'P', 'SMART')
        elif asset_class == 'Forex':
            contract = Forex(ticker)
        else:
             print('Error in identifying asset class identifier') 
            
            
        
        return contract
    

