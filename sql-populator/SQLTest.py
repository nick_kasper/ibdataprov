import pandas as pd
from sqlalchemy import create_engine
import pymysql



data = [{'Grades':4.0, 'Major':'CS', '_Class': 2022}]
dataframe = pd.DataFrame(data)

table_name = 'poo'

def make_cursor(database_name):
    text = open('keys.txt','r')
    content = str(text.read())
    user = content.split('<')[1].split('>')[0]
    password = content.split('<')[2].split('>')[0]

    db = create_engine("mysql://"+user+':'+password+"@localhost/"+database_name +"?unix_socket=/var/run/mysqld/mysqld.sock", pool_pre_ping=True)
    return db

engine = make_cursor('test')


with engine.connect() as connection:
    connection.execute("drop table poo;")
    connection.execute("SHOW TABLES;")
    connection.execute("CREATE TABLE IF NOT EXISTS poo(Grades FLOAT(2),Major VARCHAR(10), _Class INTEGER, UNIQUE(Grades, Major, _Class));")
    connection.execute("DESCRIBE poo;")
    jazz = connection.execute("SELECT * from poo")
    print (jazz.fetchone())
    for jill in jazz:
        print(jill)
        print(jazz)
    try:
        dataframe.to_sql(table_name,connection,if_exists ='append', index=False)
        connection.execute('SELECT * from poo;')
        print('did it ')
    except ValueError as vx:
        print(vx)
    except Exception as ex:
        print(ex)
    finally:
        print("Table %s created successfully."%table_name)
        connection.close()

