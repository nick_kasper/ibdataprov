# Data Provider Function


### Goals
Create a multifunctional data provider tool:
    1) Create a script that pulls marketdata, fundamental data, news, and puts
    it into a SQL database (sorted by libraries and tables)
    2) Develop an object oriented algorithm for strategists, backtesters, and feature analysts (and any other devs) to pull data a la carteo
    3) After 1 and 2 are developed, implement AFML in order to transform data in step 1 or 2 into different bar methods. 
    4) Continue adding on more features and build out more of the pipeline
